import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Article } from '@his-viewmodal/article/src';
import { DataViewModule } from 'primeng/dataview';
import { ArticleListComponent } from './article-list/article-list.component';
@Component({
  selector: 'app-root',
  standalone: true,
  imports: [CommonModule, DataViewModule,ArticleListComponent],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'articel-editor';

  article: Article = {} as Article;
}
