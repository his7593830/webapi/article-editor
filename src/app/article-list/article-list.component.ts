import { Component, inject, signal } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArticleService } from './article.service';
import { Article } from '@his-viewmodel/article/src';
import { PanelModule } from 'primeng/panel';
import { ArticleBodyComponent } from './article-body/article-body.component';
import { ArticleHeaderComponent } from './article-header/article-header.component';
import { DataViewModule } from 'primeng/dataview';

@Component({
  selector: 'app-article-list',
  standalone: true,
  imports: [CommonModule, PanelModule, ArticleBodyComponent, ArticleHeaderComponent, DataViewModule],
  providers: [ArticleService],
  templateUrl: './article-list.component.html',
  styleUrls: ['./article-list.component.scss']
})
export class ArticleListComponent {
  #articleService: ArticleService = inject(ArticleService);
  articles = signal([] as Article[]);

  ngOnInit() {
    this.#getArticles()
  }

  onRemoveArticle = async (article: Article): Promise<void> => {

    await this.#articleService.removeArticle(article.id).catch(e => e) && this.articles.update(a => a.filter(v => v.id !== article.id));
  }

  onModifyArticle = async (article: Article): Promise<void> => {

    await this.#articleService.modifyArticle(article).catch(e => e) && this.articles.update(a => a.map(v => v.id === article.id ? article : v));
  }

  #getArticles = async () => this.articles.set(await this.#articleService.getArticles());
}
