import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Article } from '@his-viewmodal/article/src';

@Component({
  selector: 'his-article-body',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './article-body.component.html',
  styleUrls: ['./article-body.component.scss']
})
export class ArticleBodyComponent {

  @Input() item!: Article;
}
