import { Injectable, inject } from '@angular/core';
import { Article } from '@his-viewmodel/article/src';
import { HttpClient } from '@angular/common/http';
import { lastValueFrom } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ArticleService implements ArticleService {

  #http = inject(HttpClient);
  #url = 'http://localhost:3001/articles';
  constructor() { }
  getArticles = (): Promise<Article[]> => lastValueFrom(this.#http.get<Article[]>(this.#url));


  removeArticle = (id: number) => lastValueFrom(this.#http.delete(`${this.#url}/${id}`));


  modifyArticle = (article: Article) => lastValueFrom(this.#http.patch(`${this.#url}/${article.id}`, article));
}
