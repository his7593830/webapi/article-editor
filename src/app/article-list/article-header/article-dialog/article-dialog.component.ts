import { Component, EventEmitter, Input, OnInit, Output, inject } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ArticleService } from '../../article.service';
import { Article } from '@his-viewmodal/article/src'

//primeng
import { InputTextModule } from 'primeng/inputtext';
import { CalendarModule } from 'primeng/calendar';
import { DialogModule } from 'primeng/dialog';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { EditorModule } from 'primeng/editor';
import { DividerModule } from 'primeng/divider';

@Component({
  selector: 'his-article-dialog',
  standalone: true,
  imports: [CommonModule, FormsModule, ReactiveFormsModule,
    //primeng
    InputTextModule, CalendarModule, DialogModule, InputTextareaModule, EditorModule, DividerModule,
  ],
  providers: [DatePipe],
  templateUrl: './article-dialog.component.html',
  styleUrls: ['./article-dialog.component.scss'],
})
export class ArticleDialogComponent implements OnInit {

  @Input() isDialogVisibled!: boolean;
  @Input() article!: Article;

  @Output() changes = new EventEmitter<Article>();
  @Output() dialogClosed = new EventEmitter();

  articleService: ArticleService = inject(ArticleService);
  originalArticle!: Article;
  datePipe: DatePipe = inject(DatePipe);
  date!: Date;


  ngOnInit(): void {

    this.originalArticle = this.article;
    this.date = new Date(this.article.date);
  }

  toggleDialog() {

    if (this.isDialogVisibled) {
      this.isDialogVisibled = false;
    }
    else {
      this.isDialogVisibled = true;
    }
  }

  doModifyArticle() {

    this.article.date = this.formatDateStr(this.date);
    this.changes.emit(this.article);
    this.toggleDialog();
  }


  formatDateStr(date: Date) {

    const year = date.getFullYear();
    const month = date.getMonth();
    const day = date.getDate();
    const hour = date.getHours();
    const min = date.getMinutes();
    return `${year}/${month}/${day} ${hour}:${min}`

  }
  doCloseDialog() {

    this.dialogClosed.emit(this.originalArticle);
    this.isDialogVisibled = false;
  }
}
